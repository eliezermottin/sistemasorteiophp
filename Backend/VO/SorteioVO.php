<?php 

class SorteioVO{
    private $id;
    private $nomeApostador;
	private $numerosSorteados;
	private $numerosApostado;

    
	/**
	 * @return mixed
	 */
	function getId() {
		return $this->id;
	}
	
	/**
	 * @param mixed $id 
	 * @return SorteioVO
	 */
	function setId($id): self {
		$this->id = $id;
		return $this;
	}
	/**
	 * @return mixed
	 */
	function getNomeApostador() {
		return $this->nomeApostador;
	}
	
	/**
	 * @param mixed $nomeApostador 
	 * @return SorteioVO
	 */
	function setNomeApostador($nomeApostador): self {
		$this->nomeApostador = $nomeApostador;
		return $this;
	}
	/**
	 * @return mixed
	 */
	function getNumerosSorteados() {
		return $this->numerosSorteados;
	}
	
	/**
	 * @param mixed $numerosSorteados 
	 * @return SorteioVO
	 */
	function setNumerosSorteados($numerosSorteados): self {
		$this->numerosSorteados = $numerosSorteados;
		return $this;
	}
	/**
	 * @return mixed
	 */
	function getNumerosApostado() {
		return $this->numerosApostado;
	}
	
	/**
	 * @param mixed $numerosApostado 
	 * @return SorteioVO
	 */
	function setNumerosApostado($numerosApostado): self {
		$this->numerosApostado = $numerosApostado;
		return $this;
	}
}


?>
