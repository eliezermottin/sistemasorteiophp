<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Sistema Sorteio</title>
    <!-- Custom fonts for this template-->
    <link href="bibliotecas/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="bibliotecas/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bibliotecas/bootstrap/js/bootstrap.js" rel="stylesheet">
    <link href="bibliotecas/bootstrap/js/bootstrap.min.js" rel="stylesheet">
    

    <script src="bibliotecas/js/jquery.js"></script>
    <script src="bibliotecas/js/jquery.min.js"></script>

    <link rel="stylesheet" type="text/css" href="bibliotecas/toast/toastr.js">
    <link rel="stylesheet" type="text/css" href="bibliotecas/toast/toastr.min.js">

    <script src="bibliotecas/js/site.js"></script>
</head>

<body id="page-top">