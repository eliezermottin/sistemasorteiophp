$(document).ready(function () {
    $('#idNumInicio').mask('00');
    $('#idNumFim').mask('00');
});

$("#btnSortear").click(function () {

    var txtLoteria = document.getElementById("idNomeLoteria").value;
    var txtData = document.getElementById("idData").value;
    var txtNumInicio = document.getElementById("idNumInicio").value;
    var txtNumFinal = document.getElementById("idNumFinal").value;


    var dataAtual = DataAtual();

    if (txtLoteria.length < 5) {
        alert("O nome da loteria nao pode ter menos que 5 caracteres.");
        return;
    }

    if (!txtData) {
        alert("data esta vazio");
        return;
    }

    if (txtData < dataAtual) {
        alert("Data é menor que a data atual!");
        return;
    }

    if (txtNumInicio < 1 || txtNumInicio > 5) {
        alert("O numero inicial deve estar entre 1 a 5");
        return;
    }

    if (txtNumFinal < 60 || txtNumFinal > 80) {
        alert("O numero final deve estar entre 60 a 80");
        return;
    }


    $.ajax({
        "url": path + '/sorteio/SorteioPost.php',
        type: 'post',
        data: {
            'txtLoteria': txtLoteria, 'txtData': txtData, 'txtNumInicio': txtNumInicio, 'txtNumFinal': txtNumFinal
        },
        success: function (output) {
            var result = JSON.parse(output);

            document.getElementById("resultados").style.display = 'block';

            var apostador1;
            if (result["Apostador1"] == "") {
                document.getElementById("resultado1").innerHTML = "Não houve acertos";
            } else {
                apostador1 = result["Apostador1"];
                document.getElementById("resultado1").innerHTML = "Numeros acertados: " + apostador1;
            }

            var apostador2;
            if (result["Apostador2"] == "") {
                console.log("Nao teve acerto")
                document.getElementById("resultado2").innerHTML = "Não houve acertos";
            } else {
                apostador2 = result["Apostador2"];
                document.getElementById("resultado2").innerHTML = "Numeros acertados: " + apostador2;
            }

            var apostador3;
            if (result["Apostador3"] == "") {
                console.log("Nao teve acerto")
                document.getElementById("resultado3").innerHTML = "Não houve acertos";
            } else {
                apostador3 = result["Apostador3"];
                document.getElementById("resultado3").innerHTML = "Numeros acertados: " + apostador3;
            }

            document.getElementById("idNomeLoteria").value = "";
            document.getElementById("idData").value = "";
            document.getElementById("idNumInicio").value = "";
            document.getElementById("idNumFinal").value = "";


        }
    });


});



function DataAtual() {
    var data = new Date();
    function adicionarUmMes() {
        var mes = data.getMonth();
        if (mes >= 0 && mes <= 11) {
            return data.getMonth() + 1;
        }
    }
    var dia = (data.getDate() < 10 ? '0' : '') + data.getDate();
    var mes = (adicionarUmMes() < 10 ? '0' : '') + adicionarUmMes();
    var ano = data.getFullYear();

    var data_atual = ano + '-' + mes + '-' + dia;

    return data_atual;
}