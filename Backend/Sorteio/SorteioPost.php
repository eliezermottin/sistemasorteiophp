<?php

require_once('configDAO.php');
require_once('configVO.php');

//$resposta = new RespostaVO();

if (!empty($_POST)) {

    $nomeLoteria = $_POST['txtLoteria'];
    $data = $_POST['txtData'];
    $numInicio = $_POST['txtNumInicio'];
    $numFinal = $_POST['txtNumFinal'];

    $numRand1 = rand($numInicio, $numFinal);
    $numRand2 = rand($numInicio, $numFinal);
    $numRand3 = rand($numInicio, $numFinal);
    $numRand4 = rand($numInicio, $numFinal);
    $numRand5 = rand($numInicio, $numFinal);
    $numRand6 = rand($numInicio, $numFinal);

    $sorteio = array($numRand1, $numRand2, $numRand3, $numRand4, $numRand5, $numRand6);

    $apostador1 = [20, 30, 50, 65, 40, 77];
    $apostador2 = [25, 35, 48, 60, 43, 78];
    $apostador3 = [29, 37, 56, 62, 49, 75];

    $apost1Count = 0;
    $acertosAP1 = [];
    foreach ($sorteio as $key) {
        foreach ($apostador1 as $p1) {
            if ($key == $p1) {
                $apost1Count += 1;
                array_push($acertosAP1, $key);
            }
        }
    }

    $apost2Count = 0;
    $acertosAP2 = [];
    foreach ($sorteio as $key) {
        foreach ($apostador2 as $p2) {
            if ($key == $p2) {
                $apost2Count += 1;
                array_push($acertosAP2, $key);
            }
        }
    }

    $apost3Count = 0;
    $acertosAP3 = [];
    foreach ($sorteio as $key) {
        foreach ($apostador3 as $p3) {
            if ($key == $p3) {
                $apost3Count += 1;
                array_push($acertosAP3, $key);
            }
        }
    }    

    $result = [
        "Apostador1" => $acertosAP1,
        "Apostador2" => $acertosAP2,
        "Apostador3" => $acertosAP3
    ];


    /*$sorteioVO = new SorteioVO();

    $sorteioVO->setNomeApostador("Apostador");
    $sorteioVO->setNumerosSorteados($sorteio);
    $sorteioVO->setNumerosApostado($acertosAP1);

    $sorteioDAO = new SorteioDAO();

    $sorteioDAO->insertSorteio($sorteioVO);*/

    echo json_encode($result);    
}
