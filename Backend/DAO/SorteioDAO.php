<?php

require_once 'Conexao.php';
require_once 'configVO.php';

class SorteioDAO {    

    //funcao que cadastra o cliente
    public function insertSorteio($sorteioVO) {
        $sql = new Conexao();
        return $sql->select("insert into sorteio (nomeApostador, numerosSorteados, numerosApostador) values (:nomeApostador, :numerosSorteados, :numerosApostador)", array(                    
                    ':nomeApostador' => $sorteioVO->getNomeApostador(),
                    ':numerosSorteados' => $sorteioVO->getNumerosSorteados(),
                    ':numerosApostador' => $sorteioVO->getNumerosApostado()
        ));
    }


}
