<?php
include_once 'header.php';
?>

<div class="container">
    <div class="row">
        <div class="col">
            <form>
                <h3>Sorteio</h3>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="idNomeLoteria">Nome Loteria</label>
                        <input type="text" class="form-control" id="idNomeLoteria" placeholder="Digite um nome">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="idData">Data Sorteio</label>
                        <input type="date" class="form-control" id="idData" >
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="idNumInicio">Numero inicial</label>
                        <input type="text" class="form-control" id="idNumInicio" >
                    </div>
                    <div class="form-group col-md-6">
                        <label for="idNumFinal">Numero Final</label>
                        <input type="text" class="form-control" id="idNumFinal" >
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-4 my-2">
                        <button type="button" class="btn btn-outline-success btn-block" id="btnSortear">Sortear</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row" id="resultados" style="display: none;">
        <div class="col-md-4">
            <h3>Apostador 1</h3>
            <span id="resultado1"></span>
        </div>
        <div class="col-md-4">
            <h3>Apostador 2</h3>
            <span id="resultado2"></span>
        </div>
        <div class="col-md-4">
            <h3>Apostador 3</h3>
            <span id="resultado3"></span>
        </div>
    </div>
</div>

<script type="text/javascript" src="jsDAO/sorteio/jsSorteio.js"></script>

<?php
include_once 'footer.php';
?>