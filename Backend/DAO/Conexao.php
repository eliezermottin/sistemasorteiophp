<?php

//namespace \DAO;

class Conexao extends PDO {

    const HOSTNAME = "localhost";
    const USERNAME = "root";
    const PASSWORD = "";
    const DBNAME = "bdsorteio";

    private $conn;

    //funcao que realiza a conexao com o banco de dados, declarada no construtor da classe Conexao
    public function __construct() {
        $this->conn = new \PDO(
                "mysql:dbname=" . Conexao::DBNAME . ";host=" . Conexao::HOSTNAME."; charset=utf8",                
                Conexao::USERNAME,
                Conexao::PASSWORD
        );
    }

    //funcao que realiza os parametros pra uma sql, mais de uma parametros
    private function setParams($statement, $parameters = array()) {
        foreach ($parameters as $key => $value) {
            $this->setParam($statement, $key, $value);
        }
    }

    //funcao que realiza o parametro para uma sql, apenas um parametro
    /* private function bindParam($statement, $key, $value) {
      $statement->bindParam($key, $value);
      } */

    private function setParam($statement, $key, $value) {
        $statement->bindParam($key, $value);
    }

    //funcao que realiza uma query no banco e executa a mesma
    public function query($rawQuery, $params = array()) {

        $stmt = $this->conn->prepare($rawQuery);

        $this->setParams($stmt, $params);

        $stmt->execute();
        
        return $stmt;
    }

    public function select($rawQuery, $params = array()): array {        
        
        $stmt = $this->query($rawQuery, $params);
       
        
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
        
    }
    
    public function selectPresenca($rawQuery, $params = array()): array {        
        
        $stmt = $this->query($rawQuery, $params);
       
        
        return $stmt->fetchAll(PDO::FETCH_COLUMN);
        
    }
    
    public function selectFunction($rawQuery, $params = array()): array {        
        
        $stmt = $this->query($rawQuery, $params);
       
        
        return $stmt->fetchAll(PDO::FETCH_COLUMN);
        
    }

    public function query2($rawQuery, $params = array()) {

        $stmt = $this->conn->prepare($rawQuery);

        $this->setParams($stmt, $params);

        if($stmt->execute()){
            return 1;
        }else{
            return 0;
        }
    }    
}