<?php
setlocale(LC_ALL, "pt_BR", "pt_BR.utf-8", "portuguese");
date_default_timezone_set("America/Sao_Paulo");
//setlocale(LC_ALL, 'pt_BR');

spl_autoload_register(function($class_name){
    
    $filename = '../VO/'.DIRECTORY_SEPARATOR.$class_name.".php";
    
    if(file_exists($filename)){
        require_once($filename);
    }
});
