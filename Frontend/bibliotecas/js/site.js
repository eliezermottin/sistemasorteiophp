//funcao que mostra a mensagem e o tipo dela, se é de erro ou de sucesso ou de alerta, sendo passado o Id do tipo e a Mensagem em si
function mostrarMensagemAlerta(id, msg) {
    if (id == 0) {
        //toastr.options.preventDuplicates = true;
        toastr.options.closeButton = true;
        toastr.options.progressBar = true;
        toastr.error(msg);
    }
    if (id == 1) {
        toastr.options.preventDuplicates = true;
        toastr.options.closeButton = true;
        toastr.options.progressBar = true;
        toastr.success(msg);
    }
    if (id == 2) {
        //toastr.options.preventDuplicates = true;
        toastr.options.closeButton = true;
        toastr.options.progressBar = true;
        toastr.warning(msg);
    }
}

/**
 * CAMINHO DO PATH PARA FAZER O CRUD DO SISTEMA
 */
 const path = "http://localhost:8081/sistemasorteiophp/Backend";